<?php
class template
{
    var $template_data = array();

    function set($content_area, $value)
    {
        $this->template_data[$content_area] = $value;
    }

    function setUserName($content_area, $value)
    {
        $this->template_data[$content_area] = $value;
    }

    // function loadview($title, $description, $shareimage, $url, $template = '', $name = '', $view = '', $view_data = array(), $return = false)
    function loadview($title, $template = '', $name = '', $view = '', $view_data = array(), $return = false)
    {
        $this->CI = &get_instance();

        //SET HEADER
        $this->template_data['title'] = $title;
        $this->template_data['isAdmin'] = $this->CI->session->userdata('user_is_admin');
        // $this->template_data['shareimage'] = $shareimage;
        // $this->template_data['url'] = $url;
        // $data['url'] = base_url() . "uclass";
        if ($this->CI->session->userdata('status') != "login") {
            redirect(base_url("login"));
        }

        $this->set($name, $this->CI->load->view($view, $view_data, true));
        $this->CI->load->view('pages/' . $template, $this->template_data);
    }
}
