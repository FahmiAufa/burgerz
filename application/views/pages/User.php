<!-- Order Management -->
<div id="userList" style="display: block;">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="user-header-title">User management</div>
            <div class="user-header-desc">Add, edit, and remove users</div>
        </div>
    </div>

    <div class="row" style="margin-top: 32px;padding-bottom:100px">
        <div class="col-12">
            <div class="user-contain-title">Users</div>
            <?php foreach ($user as $key => $value) { ?>
                <div class="user-contain-card" onclick="Edit_Click(`<?php echo $value['user_username'] ?>`, `<?php echo $value['user_name'] ?>`, `<?php echo $value['user_password'] ?>`, <?php echo $value['user_id'] ?>)">
                    <div class="d-flex justify-content-between">
                        <div class="user-contain-card-title"><?php echo $value["user_name"] ?></div>
                        <img class="user-contain-card-img" src="assets\images\arrow-right\ArrowRight@2x.png" alt="">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="fixed-bottom" style="margin:0 16px 16px 16px;">
        <div class="col-12">
            <button type="button" class="btn-fix" onclick="addNewUser_Click()">Add New User</button>
        </div>
    </div>
</div>

<!-- Order create update delete -->
<div id="navbarDetail" class="user-nav d-flex justify-content-between" style="display: none !important;">
    <div class="d-flex justify-content-left">
        <img onclick="backList_Click()" class="user-nav-img" src="assets/images/arrow-left/ArrowLeft@2x.png" alt="">
        <div class="user-nav-title">Users</div>
    </div>
    <button id="btnDelete" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" style="border: none;background-color: unset;">
        <img class="user-nav-delete" src="assets/images/delete/circular-delete@2x.png" alt="" />
    </button>
</div>

<div id="userInput" style="display: none;">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="user-header-title">User management</div>
            <div class="user-header-desc">Add, edit, and remove users</div>
        </div>
    </div>

    <div class="row" style="margin-top: 6px;padding-bottom:100px">
        <form id="form">
            <input id="inputId" name="inputId" style="display: none;">
            <div class="col-12">
                <div class="user-contain-title">UserName</div>
                <input id="inputUserName" name="inputUserName" type="text" class="user-contain-input" placeholder="Your user username" required />
            </div>
            <div class="col-12">
                <div class="user-contain-title">Name</div>
                <input id="inputName" name="inputName" type="text" class="user-contain-input" placeholder="Your user name" required />
            </div>
            <div class="col-12">
                <div class="user-contain-title">Password</div>
                <input id="inputPassword" name="inputPassword" type="password" class="user-contain-input" placeholder="Your user password" required />
            </div>
            <div class="fixed-bottom" style="margin:0 16px 16px 16px;">
                <div class="col-12">
                    <button type="submit" class="btn-fix">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" data-bs-backdrop="static" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="place-content: center;">
        <div class="modal-content">
            <div class="modal-header-order">Confirmation</div>
            <div class="modal-body-order">Are you sure you want to <br> delete this?</div>
            <div class="modal-footer-order d-flex justify-content-between">
                <button class="modal-footer-order-cancel" data-bs-dismiss="modal">Cancel</button>
                <button onclick="delete_Click()" class="modal-footer-order-delete">Delete</button>
            </div>
        </div>
    </div>
</div>

<script>
    var user_id
    $(document).ready(function() {
        ShowHeader(true)
        ShowHeaderDetail()
    });

    function ShowHeaderDetail() {
        if (IsHeader == 1) {
            $("#navbarDetail").attr("style", "display: none !important");
            $("#userList").attr("style", "display: block !important");
            $("#userInput").attr("style", "display: none !important");
        } else {
            $("#navbarDetail").attr("style", "display: flex !important");
            $("#userList").attr("style", "display: none !important");
            $("#userInput").attr("style", "display: block !important");
        }
    }
    $("#form").submit(function(e) {
        e.preventDefault();
        save_Click()
    })

    function addNewUser_Click() {
        $("#btnDelete").css('display', 'none');
        $('#inputUserName').val('');
        $('#inputName').val('');
        $('#inputPassword').val('');
        $('#inputId').val('');
        ShowHeader(false)
        ShowHeaderDetail()
    }

    function backList_Click() {
        ShowHeader(true)
        ShowHeaderDetail()
    }

    function Edit_Click(username, name, password, id) {
        $("#btnDelete").css('display', 'block');
        user_id = id;
        $('#inputUserName').val(username);
        $('#inputName').val(name);
        $('#inputPassword').val('');;
        $('#inputId').val(id);
        ShowHeader(false)
        ShowHeaderDetail()
    }

    function save_Click() {
        var A = $('#inputUserName').val();
        if (!(/^[a-zA-Z0-9.\-_$@*!]{3,30}$/.test(A))) {
            alert("cant be have space");
            return false;
        } else {
            dto = {
                "user_id": $('#inputId').val(),
                "user_username": $('#inputUserName').val(),
                "user_name": $('#inputName').val(),
                "user_email": '',
                "user_password": $('#inputPassword').val(),
            }
            $.ajax({
                type: "POST",
                url: "UserController/SaveDataUser",
                data: dto,
                dataType: "json",
                success: function(response) {
                    location.reload()
                }
            });
        }
    }

    function delete_Click() {
        dto = {
            "user_id": user_id
        }
        $.ajax({
            type: "POST",
            url: "UserController/DeleteUser",
            data: dto,
            dataType: "json",
            success: function(response) {
                location.reload()
            }
        });
    }
</script>