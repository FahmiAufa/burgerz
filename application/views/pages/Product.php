<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" /> -->
<!-- Order create update delete -->
<div id="navbarDetail" class="product-nav d-flex justify-content-between">
    <div class="d-flex justify-content-left">
        <img onclick="history.back()" class="product-nav-img" src="assets/images/arrow-left/ArrowLeft@2x.png" alt="">
        <div class="product-nav-title"><?php echo $product == null ? 'Add' : 'Edit' ?> Products</div>
    </div>
    <button type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" style="border: none;background-color: unset;">
        <img style="display: <?php echo $product == null ? 'none' : 'block' ?>;" class="product-nav-delete" src="assets/images/delete/circular-delete@2x.png" alt="" />
    </button>
</div>

<div>
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="product-header-title">Product</div>
            <div class="product-header-desc">Add, edit, and remove products</div>
        </div>
    </div>

    <div class="row" style="margin-top: 6px;padding-bottom:100px">
        <form id="form" enctype="multipart/form-data" method="POST">
            <input name="inptId" style="display: none;" value="<?php echo $product == null ? null : $product[0]["product_id"] ?>" />
            <div class="col-12">
                <div class="product-contain-title">Name</div>
                <input type="text" name="inptName" id="InputName" class="product-contain-input" placeholder="Your product name" value="<?php echo $product == null ? null : $product[0]["product_name"] ?>" required />
            </div>
            <div class="col-12">
                <div class="product-contain-title">Category</div>
                <select class="product-contain-select" name="selCatgory" id="selCatgory" value="1">
                    <?php foreach ($Category as $key => $value) { ?>
                        <option value="<?php echo $value["category_id"] ?>"><?php echo $value["category_name"] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-12">
                <div class="product-contain-title">Description</div>
                <input type="text" name="inptDesc" id="inptDesc" class="product-contain-input" placeholder="Your product description" value="<?php echo ($product == null ? null : $product[0]['product_description']) ?>" required />
            </div>
            <div class=" col-12">
                <div class="product-contain-title">Price</div>
                <input type="number" name="inptPrice" id="inptPrice" class="product-contain-input" placeholder="Your product price" value="<?php echo $product == null ? null : $product[0]["product_price"] ?>" required />
            </div>
            <div class=" col-12">
                <div class="product-contain-title">Weight</div>
                <input type="number" name="inptWeight" id="inptWeight" class="product-contain-input" placeholder="Your product weight" value="<?php echo $product == null ? null : $product[0]["product_weight"] ?>" required />
            </div>
            <div class=" col-12">
                <label class="product-contain-btn" for="inptImage">
                    Upload Image
                    <input type="file" id="inptImage" name="inptImage" style="display:none">
                </label>
                <div id="divPriview" style="text-align: center;display: <?php echo ($product == null ? 'none' : 'block') ?>">
                    <img id="imgPriview" class="product-contain-img" src="assets/images/product/<?php echo $product == null ? null : $product[0]["product_image"] ?>" alt="">
                </div>
            </div>
            <div class="fixed-bottom" style="margin:0 16px 16px 16px; display: block;">
                <div class="col-12">
                    <button type="submit" class="btn-fix">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" data-bs-backdrop="static" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="place-content: center;">
        <div class="modal-content">
            <div class="modal-header-order">Confirmation</div>
            <div class="modal-body-order">Are you sure you want to <br> delete this?</div>
            <div class="modal-footer-order d-flex justify-content-between">
                <button class="modal-footer-order-cancel" data-bs-dismiss="modal">Cancel</button>
                <button class="modal-footer-order-delete" onclick="delete_Click()">Delete</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        ShowHeader(false)
        ShowHeaderDetail()
        $('#selCatgory').val(<?php echo $product == null ? 1 : $product[0]["category_id"] ?>);
    });

    function ShowHeaderDetail() {
        if (IsHeader == 1) {
            $("#navbarDetail").attr("style", "display: none !important");
        } else {
            $("#navbarDetail").attr("style", "display: flex !important");
        }
    }

    $("#form").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        data = $('#form').serializeArray();
        $.ajax({
            type: "POST",
            url: "ProductController/CreateProduct",
            // data: dto,
            data: formData,
            contentType: false,
            processData: false,
            // dataType: "json",
            success: function(response) {
                location.href = "order"
            }
        });
    });

    function delete_Click() {
        dto = {
            "product_id": <?php echo ($product == null ? '0' : $product[0]["product_id"]) ?>
        }
        $.ajax({
            type: "POST",
            url: "ProductController/DeleteProduct",
            data: dto,
            dataType: "json",
            success: function(response) {
                location.href = 'order'
            }
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imgPriview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
            $("#divPriview").css("display", "block");
        }
    }

    $("#inptImage").change(function() {
        readURL(this);
    });
</script>