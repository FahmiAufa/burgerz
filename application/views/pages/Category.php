<!-- Order Management -->
<div id="categoryList" style="display: block;">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="category-header-title">Categories</div>
            <div class="category-header-desc">Add, edit, and remove categories</div>
        </div>
    </div>

    <div class="row" style="margin-top: 32px;padding-bottom:100px">
        <div class="col-12">
            <?php foreach ($Category as $key => $value) { ?>
                <div class="category-contain-card" onclick="Edit_Click(`<?php echo $value['category_name'] ?>`, <?php echo $value['category_weight'] ?>, <?php echo $value['category_id'] ?>)">
                    <div class="d-flex justify-content-between">
                        <div class="category-contain-card-title"><?php echo $value["category_name"] ?></div>
                        <img class="category-contain-card-img" src="assets/images/arrow-right/ArrowRight@2x.png" alt="">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="fixed-bottom" style="margin:0 16px 16px 16px;">
        <div class="col-12">
            <button type="button" class="btn-fix" onclick="addNewCategory_Click()">Add New Category</button>
        </div>
    </div>
</div>

<!-- Order create update delete -->
<div id="navbarDetail" class="category-nav d-flex justify-content-between" style="display: none !important;">
    <div class="d-flex justify-content-left">
        <img onclick="backList_Click()" class="category-nav-img" src="assets/images/arrow-left/ArrowLeft@2x.png" alt="">
        <div class="category-nav-title">Category</div>
    </div>
    <button id="btnDelete" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" style="border: none;background-color: unset;">
        <img class="category-nav-delete" src="assets/images/delete/circular-delete@2x.png" alt="" />
    </button>
</div>

<div id="categoryInput" style="display: none;">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="category-header-title">Category</div>
            <div class="category-header-desc">Enter your category name</div>
        </div>
    </div>

    <div class="row" style="margin-top: 6px;padding-bottom:100px">
        <input type="text" id="inputId" name="inputId" style="display: none;">
        <div class="col-12">
            <div class="category-contain-title">Name</div>
            <input id="inputName" name="inputName" type="text" class="category-contain-input" placeholder="Your category name" />
        </div>
        <div class="col-12">
            <div class="category-contain-title">Weight</div>
            <input id="inputWeight" name="inputWeight" type="number" class="category-contain-input" placeholder="Your category weight" />
        </div>
    </div>

    <div class="fixed-bottom" style="margin:0 16px 16px 16px;">
        <div class="col-12">
            <button type="button" class="btn-fix" onclick="save_Click()">Save</button>
        </div>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" data-bs-backdrop="static" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="place-content: center;">
        <div class="modal-content">
            <div class="modal-header-order">Confirmation</div>
            <div class="modal-body-order">Are you sure you want to <br> delete this?</div>
            <div class="modal-footer-order d-flex justify-content-between">
                <button class="modal-footer-order-cancel" data-bs-dismiss="modal">Cancel</button>
                <button class="modal-footer-order-delete" onclick="delete_Click()">Delete</button>
            </div>
        </div>
    </div>
</div>

<script>
    var category_id
    $(document).ready(function() {

    });

    function ShowHeaderDetail() {
        if (IsHeader == 1) {
            $("#navbarDetail").attr("style", "display: none !important");
            $("#categoryList").attr("style", "display: block !important");
            $("#categoryInput").attr("style", "display: none !important");

        } else {
            $("#navbarDetail").attr("style", "display: flex !important");
            $("#categoryList").attr("style", "display: none !important");
            $("#categoryInput").attr("style", "display: block !important");
        }
    }

    function addNewCategory_Click() {
        $("#btnDelete").css('display', 'none');
        $('#inputName').val('');
        $('#inputWeight').val('');
        $('#inputId').val('');
        ShowHeader(false)
        ShowHeaderDetail()
    }

    function backList_Click() {
        ShowHeader(true)
        ShowHeaderDetail()
    }

    function Edit_Click(name, weight, id) {
        $("#btnDelete").css('display', 'block');
        category_id = id;
        $('#inputName').val(name);
        $('#inputWeight').val(weight);
        $('#inputId').val(id);
        ShowHeader(false)
        ShowHeaderDetail()
    }

    function save_Click() {
        dto = {
            "category_id": $('#inputId').val(),
            "category_name": $('#inputName').val(),
            "category_weight": $('#inputWeight').val()
        }
        $.ajax({
            type: "POST",
            url: "CategoryController/SaveDataCategory",
            data: dto,
            dataType: "json",
            success: function(response) {
                location.reload()
            }
        });
    }

    function delete_Click() {
        dto = {
            "category_id": category_id
        }
        $.ajax({
            type: "POST",
            url: "CategoryController/DeleteCategory",
            data: dto,
            dataType: "json",
            success: function(response) {
                location.reload()
            }
        });

    }
</script>