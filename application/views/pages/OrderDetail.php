<div class="row" style="margin-top: 16px;">
    <div class="col-12">
        <div class="order-header-finalize-title">Finalise your order</div>
        <div class="order-header-finalize-desc">Please check you have everything, and finalise</div>
        <div style="text-align: center;">
            <button class="order-header-finalize-btn">Edit order</button>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 16px;">
    <div class="col-12">
        <div class="order-card-finalize">
            <div class="d-flex justify-content-between">
                <div class="order-card-finalize-number">Order #12421</div>
                <div class="order-card-finalize-date">Febuary 16. 2021</div>
            </div>
            <div class="order-card-finalize-status">Open order</div>
            <div style="margin-top: 12px;">
                <?php foreach (['Chicken Burger', 'French Fries', 'Pizza Carbonara'] as $key => $value) { ?>
                    <div class="d-flex justify-content-between order-card-finalize-list">
                        <div><?php echo $value ?></div>
                        <div class="d-flex justify-content-between" style="width: 35%;">
                            <div>
                                1pcs
                            </div>
                            <div>
                                $192.00
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="order-card-finalize-total" style="margin: 16px -16px 0 -16px;">
                <div class="d-flex justify-content-between order-card-finalize-list" style="border: none;">
                    <div> <img src="assets/images/location/Location@2x.png" style="width: 24px;height: 24px;" alt=""> Mile End</div>
                    <div class="d-flex justify-content-between" style="width: 35%;">
                        <div>
                            Total
                        </div>
                        <div style="font-weight: bold;">
                            $192.00
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="order-menu-title">Notes</div>
        <div class="order-card-finalize" style="height: 89px;">
            <div class="d-flex justify-content-left">
                <img class="order-note-finalize-img" src="assets/images/note/Note@2x.png" alt="">
                <div class="order-note-finalize-text">Please add your name so we know who ordered
                    and any special requests</div>
            </div>
        </div>
    </div>
</div>
<div class="fixed-bottom" style="margin:0 16px 16px 16px">
    <div class="col-12">
        <button onclick="" type="button" class="btn-fix">Finalise</button>
    </div>
</div>

<script>

</script>