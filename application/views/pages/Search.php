<div class="row" style="margin-top: 16px;">
    <div class="col-12">
        <div class="d-flex justify-content-left">
            <input id="inptSearch" class="order-header-finalize-search" type="text" placeholder="Search order" onkeyup="search_Change()">
            <img class="order-header-finalize-filter" src="assets/images/filter/filter@2x.png" alt="" onclick="ShowHideMenu()">
        </div>
    </div>

    <div class="col-12" id="filter" style="display: none;">
        <div id="list1" class="dropdown-check-list" tabindex="100" style="width: 100%;">
            <div class="anchor product-contain-select">Select Branch</div>
            <div style="overflow: scroll;height: 240px;display: contents;">
                <ul class="items">
                    <?php foreach ($user as $key => $value) { ?>
                        <li class="d-flex justify-content-between"><?php echo $value["user_name"] ?> <input class="cbCheckbox" type="checkbox" name="cbUser" value="<?php echo $value["user_id"] ?>" onclick="search_Change();" /></li>
                    <?php } ?>
                </ul>
            </div>
        </div>

        <div id="list2" class="dropdown-check-list" tabindex="100" style="width: 100%;">
            <div class="anchor product-contain-select">Select Orders</div>
            <div style="overflow: scroll;height: 240px;display: contents;">
                <ul class="items">
                    <?php foreach ($status as $key => $value) { ?>
                        <li class="d-flex justify-content-between"><?php echo $value["status_name"] ?><input class="cbCheckbox" type="checkbox" name="cbStatus" value="<?php echo $value["status_id"]  ?>" onclick="search_Change();" /></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

    <!-- <div class="col-12" id="filter" style="display: none;">
        <select class="product-contain-select" name="user_id" id="user_id" onchange="search_Change()">
            <option value="0">Select Branch</option>
            <?php foreach ($user as $key => $value) { ?>
                <option value="<?php echo $value["user_id"] ?>"><?php echo $value["user_name"] ?></option>
            <?php } ?>
        </select>
        <select class="product-contain-select" name="status_id" id="status_id" onchange="search_Change()">
            <option value="0">Select Orders</option>
            <?php foreach ($status as $key => $value) { ?>
                <option value="<?php echo $value["status_id"] ?>"><?php echo $value["status_name"] ?></option>
            <?php } ?>
        </select>
    </div> -->
</div>

<div class="row" style="margin-top: 16px;">
    <div class="product-contain-title">Orders</div>
    <div id="listOrder">
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
    var isShow;
    var userArray = [];
    var statusArray = [];


    function ShowHideMenu() {
        if (isShow == true) {
            $("#filter").css("display", "none");
            isShow = false
        } else {
            $("#filter").css("display", "block");
            isShow = true
        }
    }

    function search_Change() {
        userArray = [];
        statusArray = [];
        $("input:checkbox[name=cbUser]:checked").each(function() {
            userArray.push($(this).val());
        });

        $("input:checkbox[name=cbStatus]:checked").each(function() {
            statusArray.push($(this).val());
        });

        dto = {
            "input_search": $("#inptSearch").val(),
            "status_id": statusArray.toString(),
            "user_id": userArray.toString(),
        }

        $.ajax({
            type: "POST",
            url: "SearchController/SearchDataOrder",
            data: dto,
            dataType: "json",
            success: function(response) {
                $("#listOrder").empty();
                $.each(response, function(indexInArray, valueOfElement) {

                    $data = `<div class="col-12">
                        <div class="order-card-finalize">
                            <div class="d-flex justify-content-between">
                                <div class="order-card-finalize-number">Order #` + valueOfElement["orders_code"] + `</div>
                                <div class="order-card-finalize-date">` + moment(valueOfElement["orders_paid_date"]).format("MMMM DD. Y") + `</div>
                            </div>
                            <div class="order-card-finalize-status">` + (valueOfElement["status_id"] == 1 ? 'Open Order' : 'Order Complete') + `</div>
                            <div class="order-card-finalize-total" style="margin: 16px -16px 0 -16px;">
                                <div class="d-flex justify-content-between order-card-finalize-list" style="border: none;">
                                    <div> <img src="assets/images/location/Location@2x.png" style="width: 24px;height: 24px;" alt=""> Mile End</div>
                                    <div class="d-flex justify-content-between" style="width: 35%;">
                                        <div>
                                            Total
                                        </div>
                                        <div style="font-weight: bold;">
                                            $` + parseInt(valueOfElement["orders_price"]).toFixed(2) + `
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="margin: 12px 0">
                                <button class="order-card-finalize-btn" style="width: 100%;" onclick="location.href='order-recieved?id=` + valueOfElement["orders_id"] + `'">View</button>
                            </div>
                        </div>
                    </div>`
                    $("#listOrder").append($data);

                });
            }
        });
    }
</script>

<script>
    var checkList = document.getElementById('list1');
    checkList.getElementsByClassName('anchor')[0].onclick = function(evt) {
        if (checkList.classList.contains('visible'))
            checkList.classList.remove('visible');
        else
            checkList.classList.add('visible');
    }

    var checkList2 = document.getElementById('list2');
    checkList2.getElementsByClassName('anchor')[0].onclick = function(evt) {
        if (checkList2.classList.contains('visible'))
            checkList2.classList.remove('visible');
        else
            checkList2.classList.add('visible');
    }
</script>

<style>
    .cbCheckbox {
        width: 18px;
        height: 18px;
        flex-grow: 0;
        margin: 0 3px 3px 0;
        background-color: #b4c0d3;
    }

    .dropdown-check-list {
        display: inline-block;
    }

    .dropdown-check-list .anchor {
        position: relative;
        /* cursor: pointer;
        display: inline-block;
        padding: 5px 50px 5px 10px;
        border: 1px solid #ccc; */
    }

    .dropdown-check-list .anchor:after {
        position: absolute;
        content: "";
        border-left: 2px solid #b4c0d3;
        border-top: 2px solid #b4c0d3;
        padding: 3px;
        right: 20px;
        top: 35%;
        -moz-transform: rotate(-135deg);
        -ms-transform: rotate(-135deg);
        -o-transform: rotate(-135deg);
        -webkit-transform: rotate(-135deg);
        transform: rotate(-135deg);
    }

    .dropdown-check-list .anchor:active:after {
        right: 8px;
        top: 21%;
    }

    .dropdown-check-list ul.items {
        padding: 8px;
        display: none;
        margin: 0;
        border: none;
        border-top: none;
    }

    .dropdown-check-list ul.items li {
        font-size: 14px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: left;
        color: #161938;
        box-shadow: 0 0 16px 0 rgb(22 25 56 / 5%);
        padding: 16px 15px;
        list-style: none;
        border-radius: 16px;
        margin-top: 1px;
        background-color: #ffffff;
    }

    .dropdown-check-list.visible .items {
        display: block;
    }
</style>