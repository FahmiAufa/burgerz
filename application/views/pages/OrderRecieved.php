<!-- <div id="navbarDetail" class="category-nav d-flex justify-content-between">
    <div class="d-flex justify-content-left">
        <img onclick="history.back()" class="category-nav-img" src="assets/images/arrow-left/ArrowLeft@2x.png" alt="">
        <div class="category-nav-title">Orders</div>
    </div>
    <button id="btnDelete" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" style="border: none;background-color: unset;">
        <img onclick="share_Click()" class="category-nav-img" src="assets/images/share/arrow_circle_up@2x.png" alt="">
        <img class="category-nav-delete" src="assets/images/delete/circular-delete@2x.png" alt="" />
    </button>
</div> -->

<div class="row" style="margin-top: 16px;">
    <div class="col-12">
        <div class="order-header-finalize-title">Mile End Order</div>
        <div class="order-header-finalize-desc">Once sent please mark as complete</div>
        <!-- <div style="text-align: center;">
            <button class="order-header-finalize-btn">Edit order</button>
        </div> -->
    </div>
</div>

<div class="row" style="margin-top: 16px;">
    <div class="col-12">
        <div class="order-card-finalize">
            <div class="d-flex justify-content-between">
                <div class="order-card-finalize-number">Order #<?php echo $order[0]["orders_code"] ?></div>
                <div class="order-card-finalize-date"><?php echo date_format(date_create($order[0]["orders_paid_date"]), "M d. Y")  ?></div>
            </div>
            <div class="order-card-finalize-status"><?php echo ($order[0]["status_id"] == 1 ? "Open order" : "Order Complete") ?></div>
            <div style="margin-top: 12px;">
                <?php foreach ($orderTrans as $key => $value) { ?>
                    <div class="d-flex justify-content-between order-card-finalize-list">
                        <div><?php echo $value["product_name"] ?></div>
                        <div class="d-flex justify-content-between" style="width: 35%;">
                            <div>
                                <?php echo $value["orders_qty"] ?>pcs
                            </div>
                            <div>
                                $<?php echo number_format(($value["product_price"] * $value["orders_qty"]), 2) ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="order-card-finalize-total" style="margin: 16px -16px 0 -16px;">
                <div class="d-flex justify-content-between order-card-finalize-list" style="border: none;">
                    <div> <img src="assets/images/location/Location@2x.png" style="width: 24px;height: 24px;" alt=""> Mile End</div>
                    <div class="d-flex justify-content-between" style="width: 35%;">
                        <div>
                            Total
                        </div>
                        <div style="font-weight: bold;">
                            $<?php echo number_format($order[0]["orders_price"], 2) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="order-menu-title">Notes</div>
        <div class="order-card-finalize" style="height: 89px;">
            <div class="d-flex justify-content-left">
                <img class="order-note-finalize-img" src="assets/images/note/Note@2x.png" alt="">
                <?php if ($order[0]["orders_notes"] == "") { ?>
                    <div class="order-note-finalize-text">Please add your name so we know who ordered
                        and any special requests</div>
                <?php } else { ?>
                    <div class="order-note-finalize-text"><?php echo $order[0]["orders_notes"] ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-12" style="padding-bottom: 100px;display: <?php echo ($order[0]["status_id"] == 1 && $this->session->userdata('user_is_admin') == 1 ? "block" : "none") ?>;">
        <div class="order-menu-title">Order Status</div>
        <div class="order-card-finalize" style="height: 89px;">
            <?php foreach ($status as $key => $value) {  ?>
                <input type="radio" id="id-<?php echo $value["status_id"] ?>" name="status" value="<?php echo $value["status_id"] ?>">
                <label for="<?php echo $value["status_id"] ?>"><?php echo $value["status_name"] ?></php></label><br>
            <?php } ?>
        </div>
    </div>
</div>
<div class="fixed-bottom" style="margin:0 16px 16px 16px;display: <?php echo ($order[0]["status_id"] == 1 && $this->session->userdata('user_is_admin') == 1 ? "block" : "none") ?>;">
    <div class="col-12">
        <button type="button" class="btn-fix" onclick="update_Click()">Update</button>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" data-bs-backdrop="static" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="place-content: center;">
        <div class="modal-content">
            <div class="modal-header-order">Confirmation</div>
            <div class="modal-body-order">Are you sure you want to <br> delete this?</div>
            <div class="modal-footer-order d-flex justify-content-between">
                <button class="modal-footer-order-cancel" data-bs-dismiss="modal">Cancel</button>
                <button class="modal-footer-order-delete" onclick="delete_Click()">Delete</button>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#id-<?php echo $order[0]["status_id"] ?>").prop("checked", true);
        });

        function update_Click() {
            dto = {
                "status_id": $("input[name='status']:checked").val(),
                "orders_id": <?php echo $order[0]["orders_id"] ?>
            }
            $.ajax({
                type: "POST",
                url: "OrderRecievedController/UpdateDataOrder",
                data: dto,
                dataType: "json",
                success: function(response) {
                    location.href = 'search'
                }
            });
        }

        function delete_Click() {
            dto = {
                "orders_id": <?php echo $order[0]["orders_id"] ?>
            }
            $.ajax({
                type: "POST",
                url: "OrderRecievedController/DeleteOrder",
                data: dto,
                dataType: "json",
                success: function(response) {
                    location.href = 'search'
                }
            });

        }
    </script>