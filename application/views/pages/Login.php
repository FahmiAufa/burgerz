<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- jquery js -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>

    <!-- Global CSS -->
    <link href="assets/css/global.css" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <title>Burgerz</title>
</head>

<body>
    <div class="container">
        <div class="row" style="margin-top:64px">
            <div class="col-12" style="text-align: center;">
                <img class="login-header-img" src="assets/images/logo-small/6burgerssmall@2x.png" alt="Logo Login">
            </div>
        </div>
        <form class="form" method="post" action="LoginController/UserLogin">
            <div class="row" style="margin-top:35px">
                <div class="col-12">
                    <div class="login-header-title">Login</div>
                    <div class="login-header-desc">Use your credentials or request one from headoffice.</div>
                </div>
            </div>
            <div class="row" style="margin-top:17px">
                <div class="col-12">
                    <div class="login-form-name">Username</div>
                    <input name="inptUserName" id="inptUserName" type="text" class="login-form-input" placeholder="Enter your name" />
                </div>
                <div class="col-12">
                    <div class="login-form-name">Password</div>
                    <input name="inptPassword" id="inptPassword" type="password" class="login-form-input" placeholder="Enter your password" />
                </div>
                <div class="form-group" id="error" style=" text-align: center;">

                </div>
                <div class="col-12" style="margin-top:15px">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            Remember me
                        </label>
                    </div>
                </div>
            </div>
            <div class="fixed-bottom" style="margin:0 16px 16px 16px">
                <div class="col-12">
                    <button type="button" class="btn-fix" onclick="btnLogin_Click()">Login</button>
                </div>
            </div>
        </form>
    </div>

    <script>
        function btnLogin_Click() {
            $("#errorform").remove();

            UserName = $("#inptUserName").val();
            Password = $("#inptPassword").val();

            if (UserName == "") {
                $("#error").append('<div id="errorform"><label style="color: red;">*please fill in your user name</label></div>');
            } else if (Password == "") {
                $("#error").append('<div id="errorform"><label style="color: red;">*please fill in your password</label></div>');
            } else {
                dto = {
                    "inptUserName": UserName,
                    "inptPassword": Password
                }
                $.ajax({
                    type: 'POST',
                    crossDomain: true,
                    data: dto,
                    // url: 'https://tracker.brunbrunparis.com/ReEmailVoucherApp',
                    url: 'LoginController/UserLogin',
                    success: function(data) {
                        if (data == "Incorrect username or password") {
                            $("#error").append('<div id="errorform"><label style="color: red;">*' + data + '</label></div>');
                        } else if (data == "Correct") {
                            window.location.href = "order";
                        }
                    }
                })
            }
        }
    </script>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="assets/js/bootstrap.bundle.js"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    -->
</body>

</html>