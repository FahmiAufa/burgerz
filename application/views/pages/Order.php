<!-- ORDER -->
<div id="order" style="display:<?php echo ($this->session->userdata('user_is_admin') == 1 ? 'none' : 'block') ?>">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="order-header-title">Hello <?php echo $this->session->userdata('user_name'); ?>,</div>
            <div class="order-header-desc">All our stock in one place,</div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px; padding-bottom: 100px;">
        <div class="col-12">
            <?php foreach ($Category as $key => $valueCategory) { ?>
                <div class="order-menu-title"><?php echo $valueCategory["category_name"] ?></div>
                <?php foreach ($Product as $key => $valueProduct) { ?>
                    <?php if ($valueProduct["category_id"] == $valueCategory["category_id"]) { ?>
                        <div class="order-menu-card" style="margin-top: 12px;">
                            <div class="d-flex justify-content-between">
                                <img class="order-menu-img" src="" alt="">
                                <div class="d-flex flex-column order-menu-text" style="width: 100%;margin: 0 12px;">
                                    <div><?php echo $valueProduct["product_name"] ?></div>
                                    <div>$<?php echo $valueProduct["product_price"] ?></div>
                                </div>
                                <div id="div-btn-<?php echo $valueProduct["product_id"] ?>" style="align-self: center;">
                                    <button id="btn-<?php echo $valueProduct["product_id"] ?>" onclick="add_click(<?php echo $valueProduct['product_id'] ?>, <?php echo $valueProduct['product_price'] ?> , `<?php echo $valueProduct['product_name'] ?>`)" class="order-menu-button">Add</button>
                                </div>
                                <div id="div-inpt-<?php echo $valueProduct["product_id"] ?>" style="align-self: center;display:none">
                                    <input type="number" value="1" min="1" id="input-<?php echo $valueProduct["product_id"] ?>" onchange="add_change(<?php echo $valueProduct['product_id'] ?>, <?php echo $valueProduct['product_price'] ?> , `<?php echo $valueProduct['product_name'] ?>`)" class="order-menu-input" />
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="fixed-bottom" style="margin:0 16px 16px 16px">
        <div class="col-12">
            <button type="button" onclick="checkout_click()" class="btn-fix">Checkout - <span id="spanTotal">$0</span> </button>
        </div>
    </div>
</div>

<div id="order" style="display:<?php echo ($this->session->userdata('user_is_admin') == 1 ? 'block' : 'none') ?>">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="order-header-title">Hello <?php echo $this->session->userdata('user_name'); ?>,</div>
            <div class="order-header-desc">All the amazing items we stock are here,</div>
        </div>
    </div>
    <div class="row" style="margin-top: 20px; padding-bottom: 100px;">
        <div class="col-12">
            <?php foreach ($Category as $key => $valueCategory) { ?>
                <div class="order-menu-title"><?php echo $valueCategory["category_name"] ?></div>
                <?php foreach ($Product as $key => $valueProduct) { ?>
                    <?php if ($valueProduct["category_id"] == $valueCategory["category_id"]) { ?>
                        <div class="order-menu-card" style="margin-top: 12px;" onclick="Edit_Click(<?php echo $valueProduct['product_id']  ?>)">
                            <div class="d-flex justify-content-between">
                                <img class="order-menu-img" src="assets/images/product/<?php echo $valueProduct['product_image']  ?>" alt="">
                                <div class="d-flex flex-column order-menu-text" style="width: 100%;margin: 0 12px;">
                                    <div><?php echo $valueProduct["product_name"] ?></div>
                                    <div>$<?php echo $valueProduct["product_price"] ?></div>
                                </div>
                                <img src="assets/images/arrow-right/ArrowRight@2x.png" alt="" style="width: 24px;height: 24px;align-self: center;">
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="fixed-bottom" style="margin:0 16px 16px 16px">
        <div class="col-12">
            <button type="button" onclick="AddNewProduct_click()" class="btn-fix">Add New Product</button>
        </div>
    </div>
</div>

<div id="order-finalise" style="display: none;">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="order-header-finalize-title">Finalise your order</div>
            <div class="order-header-finalize-desc">Please check you have everything, and finalise</div>
            <div style="text-align: center;">
                <button class="order-header-finalize-btn" onclick="editOrder_click()">Edit order</button>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="order-card-finalize">
                <div class="d-flex justify-content-between">
                    <div class="order-card-finalize-number">Order</div>
                    <div class="order-card-finalize-date"><?php echo date("M d. Y") ?></div>
                </div>
                <div class="order-card-finalize-status">Open order</div>
                <div id="listOrder" style="margin-top: 12px;">

                </div>
                <div class="order-card-finalize-total" style="margin: 16px -16px 0 -16px;">
                    <div class="d-flex justify-content-between order-card-finalize-list" style="border: none;">
                        <div> <img src="assets/images/location/Location@2x.png" style="width: 24px;height: 24px;" alt=""> Mile End</div>
                        <div class="d-flex justify-content-between" style="width: 35%;">
                            <div>
                                Total
                            </div>
                            <div id="totalOrder" style="font-weight: bold;">
                                $0.00
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="order-menu-title">Notes</div>
            <div class="order-card-finalize" style="height: 89px;">
                <div class="d-flex justify-content-left">
                    <img class="order-note-finalize-img" src="assets/images/note/Note@2x.png" alt="">
                    <textarea id="txtNotes" class="order-note-finalize-text" placeholder="Please add your name so we know who ordered and any special requests"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="fixed-bottom" style="margin:0 16px 16px 16px">
        <div class="col-12">
            <button type="button" class="btn-fix" onclick="finalise_Click()">Finalise</button>
        </div>
    </div>
</div>

<script>
    var spanTotal = 0
    var arrayTotal = []

    Array.prototype.sum = function(prop) {
        var total = 0
        for (var i = 0, _len = this.length; i < _len; i++) {
            total += this[i][prop]
        }
        return total
    }


    function add_click(id, price, name) {
        $("#div-btn-" + id).css('display', "none");
        $("#div-inpt-" + id).css('display', "block");

        var productData = {
            "product_id": id,
            "product_name": name,
            "orders_qty": 1,
            "product_price": price * 1
        };
        arrayTotal.push(productData)
        spanTotal = arrayTotal.sum("product_price")
        $("#spanTotal").text('$' + spanTotal);
    }

    function add_change(id, price, name) {
        if ($('#input-' + id).val() == 0 || $('#input-' + id).val() == "") {
            $("#div-btn-" + id).css('display', "block");
            $("#div-inpt-" + id).css('display', "none");
            var index = arrayTotal.map(function(x) {
                return x.product_id;
            }).indexOf(id);

            arrayTotal = arrayTotal.slice(index);
        } else {
            var qty = ($("#input-" + id).val())
            if (typeof arrayTotal.find(o => o.product_id === id) == "undefined") {
                var productData = {
                    "product_id": id,
                    "product_name": name,
                    "orders_qty": qty,
                    "product_price": price * qty
                };
                arrayTotal.push(productData)
            } else {
                arrayTotal.find(o => o.product_id === id).product_name = name
                arrayTotal.find(o => o.product_id === id).orders_qty = qty
                arrayTotal.find(o => o.product_id === id).product_price = price * qty
            }
        }
        spanTotal = arrayTotal.sum("product_price")
        $("#spanTotal").text('$' + spanTotal);
    }

    function addListFinalise() {
        returnData = ""
        $.each(arrayTotal, function(indexInArray, valueOfElement) {
            data = `<div class="d-flex justify-content-between order-card-finalize-list">
                            <div>` + valueOfElement.product_name + `</div>
                            <div class="d-flex justify-content-between" style="width: 35%;">
                                <div>
                                    ` + valueOfElement.orders_qty + `pcs
                                </div>
                                <div>
                                    $` + valueOfElement.product_price.toFixed(2) + `
                                </div>
                            </div>
                        </div>`
            returnData += data
        });
        $("#listOrder").html(returnData);
        $("#totalOrder").html('$' + spanTotal.toFixed(2));

    }

    function checkout_click() {
        if (arrayTotal.length != 0) {
            addListFinalise()
            $("#order").css("display", "none");
            $("#order-finalise").css("display", "block");
        } else {
            alert("please choose minimal 1 product")
        }
    }

    function editOrder_click() {
        $("#order").css("display", "block");
        $("#order-finalise").css("display", "none");
    }

    function finalise_Click() {
        dto = {
            "order_trans": arrayTotal,
            "order_price": spanTotal,
            "order_notes": $("#txtNotes").val()
        }
        $.ajax({
            type: "POST",
            url: "OrderController/SaveOrder",
            data: dto,
            dataType: "json",
            success: function(response) {
                location.href = "order-status?type=1"
            }
        });
    }

    function Edit_Click(id) {
        location.href = "product?id=" + id
    }

    function AddNewProduct_click() {
        location.href = "product"
    }
</script>