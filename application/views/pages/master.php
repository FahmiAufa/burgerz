<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- jquery js -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>

    <!-- Global CSS -->
    <link href="assets/css/global.css" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <title>Burgerz - <?php echo $title; ?></title>
</head>

<body>
    <nav id="navbar" class="navbar navbar-expand-lg navbar-light" style="margin-top: 16px;">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation" style="border: none">
                <img src="assets/images/nav-icon/Menu@2x.png" alt="" style="width: 24px;height: 24px;object-fit: contain;">
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0" style="margin-top: 28px;">
                    <?php if ($isAdmin == 1) { ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($title == "Search" ? 'active' : '') ?> nav-text" aria-current="page" href="search">Orders</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($title == "Order" ? 'active' : '') ?> nav-text" aria-current="page" href="order">Products</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($title == "Category" ? 'active' : '') ?> nav-text" aria-current="page" href="category">Categories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($title == "User" ? 'active' : '') ?> nav-text" aria-current="page" href="user">Users</a>
                        </li>
                    <?php } else {  ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($title == "Order Status" ? 'active' : '') ?> nav-text" aria-current="page" href="order-status?type=2">Past Order</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($title == "Order" ? 'active' : '') ?> nav-text" aria-current="page" href="order">Home</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link <?php echo ($title == "Profile" ? 'active' : '') ?> nav-text" aria-current="page" href="#">Profile</a>
                        </li> -->
                    <?php }  ?>
                    <li class="nav-item">
                        <a class="nav-link nav-text" aria-current="page" href="LoginController/logout" style="color: #e52a34;">logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <?php echo $contents; ?>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="assets/js/bootstrap.bundle.js"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    -->
</body>
<script>
    var IsHeader = 1

    function ShowHeader(isStatus) {
        if (isStatus == true) {
            $("#navbar").css("display", "block");
            IsHeader = 1
        } else {
            IsHeader = 0
            $("#navbar").css("display", "none");
        }

    }
</script>

</html>