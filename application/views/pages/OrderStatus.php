<!-- Order proceed -->
<div style="display: <?php echo ($type == 1 ? 'block' : 'none') ?>;">
    <div class="row" style="margin-top: 16px;">
        <div class="col-12">
            <div class="order-header-finalize-title">You have <?php echo count($orders) ?> order</div>
            <div class="order-header-finalize-desc">Your open orders can be updated or deleted</div>
        </div>
    </div>

    <div style="padding-bottom: 100px;">
        <?php foreach ($orders as $key => $value) { ?>
            <div class="row" style="margin-top: 16px;">
                <div class="col-12">
                    <div class="order-card-finalize">
                        <div class="d-flex justify-content-between">
                            <div class="order-card-finalize-number">Order #<?php echo $value["orders_code"] ?></div>
                            <div class="order-card-finalize-date"><?php echo date_format(date_create($value["orders_paid_date"]), "M d. Y")  ?></div>
                        </div>
                        <div class="order-card-finalize-status"><?php echo $value["status_id"] == 1 ? "Open order" : "" ?></div>
                        <div class="order-card-finalize-total" style="margin: 16px -16px 0 -16px;">
                            <div class="d-flex justify-content-between order-card-finalize-list" style="border: none;">
                                <div> <img src="assets/images/location/Location@2x.png" style="width: 24px;height: 24px;" alt=""> Mile End</div>
                                <div class="d-flex justify-content-between" style="width: 35%;">
                                    <div>
                                        Total
                                    </div>
                                    <div style="font-weight: bold;">
                                        $<?php echo number_format($value["orders_price"], 2) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-left" style="margin: 12px 0">
                            <button class="order-card-finalize-btn" onclick="view_Click(<?php echo $value['orders_id'] ?>)">View</button>
                            <button type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" style="border: none;background-color: unset;" onclick="Modal_Click(<?php echo $value['orders_id'] ?>)">
                                <img src="assets/images/delete/circular-delete.png" alt="" class="order-card-finalize-delete" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="fixed-bottom" style="margin:0 16px 16px 16px">
        <div class="col-12">
            <button onclick="addNewOrder_click()" type="button" class="btn-fix">Add new order</button>
        </div>
    </div>
</div>


<!-- Order Past -->
<div style="display: <?php echo ($type == 2 ? 'block' : 'none') ?>;">
    <div class="row" style="margin-top: 16px;margin-bottom: 16px;">
        <div class="col-12">
            <div class="order-header-finalize-title">Past Orders</div>
            <div class="order-header-finalize-desc">Your open orders can be updated or deleted</div>
        </div>
    </div>
    <div style="padding-bottom: 100px ;">
        <?php foreach ($ordersPast as $key => $value) { ?>
            <div class="row" style="margin-top: 8px;">
                <div class="col-12">
                    <div class="order-card-finalize">
                        <div class="d-flex justify-content-between">
                            <div class="order-card-finalize-number">Order #<?php echo $value["orders_code"] ?></div>
                            <div class="order-card-finalize-date"><?php echo date_format(date_create($value["orders_paid_date"]), "M d. Y")  ?></div>
                        </div>
                        <div class="order-card-finalize-status"><?php echo $value["status_id"] == 2 ? "Complate Order" : "" ?></div>
                        <div class="order-card-finalize-total" style="margin: 16px -16px 0 -16px;">
                            <div class="d-flex justify-content-between order-card-finalize-list" style="border: none;">
                                <div> <img src="assets/images/location/Location@2x.png" style="width: 24px;height: 24px;" alt=""> Mile End</div>
                                <div class="d-flex justify-content-between" style="width: 35%;">
                                    <div>
                                        Total
                                    </div>
                                    <div style="font-weight: bold;">
                                        $<?php echo number_format($value["orders_price"], 2) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-left" style="margin: 12px 0">
                            <button class="order-card-finalize-btn" onclick="view_Click(<?php echo $value['orders_id'] ?>)">View</button>
                            <img src="assets/images/share/arrow_circle_up@2x.png" alt="" class="order-card-finalize-delete" />
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="fixed-bottom" style="margin:0 16px 16px 16px">
        <div class="col-12">
            <button type="button" class="btn-fix">Add new order</button>
        </div>
    </div>
</div>

<!-- MODAL -->
<div class="modal fade" data-bs-backdrop="static" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="place-content: center;">
        <div class="modal-content">
            <div class="modal-header-order">Confirmation</div>
            <div class="modal-body-order">Are you sure you want to <br> delete this?</div>
            <div class="modal-footer-order d-flex justify-content-between">
                <button class="modal-footer-order-cancel" data-bs-dismiss="modal">Cancel</button>
                <button class="modal-footer-order-delete" onclick="delete_Click()">Delete</button>
            </div>
        </div>
    </div>
</div>

<script>
    var orders_id

    function Modal_Click(id) {
        orders_id = id
    }

    function addNewOrder_click() {
        location.href = "order"
    }

    function delete_Click() {
        dto = {
            "orders_id": orders_id
        }
        $.ajax({
            type: "POST",
            url: "OrderStatusController/DeleteOrder",
            data: dto,
            dataType: "json",
            success: function(response) {
                location.reload()
            }
        });

    }

    function view_Click(id) {
        location.href = 'order-recieved?id=' + id
    }
</script>