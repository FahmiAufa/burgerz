<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OrderStatusController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OrderStatusModel', 'MOrderStatus');
    }

    public function index()
    {
        $type = $this->input->get("type");
        $title = "Order Status";
        $data = array();
        $data["orders"] = $this->MOrderStatus->GetDataOrder($type);
        $data["ordersPast"] = $this->MOrderStatus->GetDataOrder($type);
        $data["type"] = $type;
        $this->template->loadview($title, 'master', 'contents', 'pages/OrderStatus', $data);
    }

    public function DeleteOrder()
    {
        $data = $this->input->post();
        $this->MOrderStatus->DeleteDataOrder($data["orders_id"]);
        echo json_encode("success");
    }
}
