<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('LoginModel', 'MLogin');
    }

    public function index()
    {
        $this->load->view("pages/Login");
    }

    function UserLogin()
    {
        $userName = $this->input->post('inptUserName');
        $password = $this->input->post('inptPassword');
        $where = array(
            'user_username' => $userName,
            'user_password' => md5($password),
            'is_active' => 1
        );
        $isRegistered = $this->MLogin->Check_Registered($where)->num_rows();
        if ($isRegistered > 0) {
            $DataUsers = $this->MLogin->GetUsers($where);
            $DataUsers = $DataUsers[0] + array('status' => "login");

            $this->session->set_userdata($DataUsers);

            echo "Correct";
        } else {
            echo "Incorrect username or password";
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }
}
