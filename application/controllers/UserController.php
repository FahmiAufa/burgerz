<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel', 'MUser');
    }

    public function index()
    {
        $title = "User";
        $data = array();
        $data["user"] = $this->MUser->GetDataUser();
        $this->template->loadview($title, 'master', 'contents', 'pages/User', $data);
    }

    public function SaveDataUser()
    {
        $data = $this->input->post();
        if ($data["user_id"] != null) {
            $arrayData = array(
                "user_id" => $data["user_id"],
                "user_username" => $data["user_username"],
                "user_name" => $data["user_name"],
                "user_email" => $data["user_email"],
                "user_password" => md5($data["user_password"]),
                "user_is_admin" => 0,
                "updated_by" => $this->session->userdata('user_username'),
                "updated_date" => date("Y/m/d h:i:sa")
            );
            $this->MUser->UpdateDataUser($arrayData);
        } else {
            $arrayData = array(
                "user_id" => $data["user_id"],
                "user_username" => $data["user_username"],
                "user_name" => $data["user_name"],
                "user_email" => $data["user_email"],
                "user_password" => md5($data["user_password"]),
                "user_is_admin" => 0,
                "is_active" => 1,
                "created_by" => $this->session->userdata('user_username'),
                "created_date" => date("Y/m/d h:i:sa"),
            );
            $this->MUser->CreateDataUser($arrayData);
        }

        echo json_encode("success");
    }

    public function DeleteUser()
    {
        $data = $this->input->post();
        $this->MUser->DeleteDataUser($data["user_id"]);

        echo json_encode("success");
    }
}
