<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OrderController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OrderModel', 'MOrder');
    }

    public function index()
    {
        $title = "Order";
        $data = array();
        $data["Category"] = $this->MOrder->GetDataCategory();
        $data["Product"] = $this->MOrder->GetDataProduct();
        $this->template->loadview($title, 'master', 'contents', 'pages/Order', $data);
    }

    public function SaveOrder()
    {
        $data = $this->input->post();
        $dataArray = array(
            "orders_id" => null,
            "orders_code" => "",
            "user_id" => $this->session->userdata('user_id'),
            "status_id" => 1,
            "orders_price" => $data["order_price"],
            "orders_paid_date" => date("Y/m/d h:i:sa"),
            "orders_notes" => $data["order_notes"],
            "is_active" => 1,
            "created_by" => $this->session->userdata('user_username'),
            "created_date" => date("Y/m/d h:i:sa")
        );
        $orders_id = $this->MOrder->SaveOrder($dataArray);

        foreach ($data["order_trans"] as $key => $value) {
            $dataArray = array(
                "orders_trans_id" => null,
                "orders_id" => $orders_id,
                "product_id" => $value["product_id"],
                "orders_qty" => $value["orders_qty"],
                "is_active" => 1,
                "created_by" => $this->session->userdata('user_username'),
                "created_date" => date("Y/m/d h:i:sa"),
            );
            $this->MOrder->SaveOrderTrans($dataArray);
        }

        echo json_encode("success");
    }
}
