<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OrderRecievedController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SearchModel', 'MSearch');
        $this->load->model('OrderRecievedModel', 'MOrderRecieved');
    }

    public function index()
    {
        $id = $this->input->get("id");
        $title = "Order Recieved";
        $data = array();
        $data['order'] = $this->MOrderRecieved->GetDataOrder($id);
        $data['orderTrans'] = $this->MOrderRecieved->GetDataOrderTrans($id);
        $data['status'] = $this->MSearch->GetDataStatus();
        $this->template->loadview($title, 'master', 'contents', 'pages/OrderRecieved', $data);
    }

    public function UpdateDataOrder()
    {
        $data = $this->input->post();
        $this->MOrderRecieved->UpdateDataOrder($data["status_id"], $data["orders_id"]);
        echo 'success';
    }

    public function DeleteOrder()
    {
        $data = $this->input->post();
        $this->MOrderRecieved->DeleteDataOrder($data["orders_id"]);
        echo json_encode("success");
    }
}
