<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SearchController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SearchModel', 'MSearch');
    }

    public function index()
    {
        $title = "Search";
        $data = array();
        $data['status'] = $this->MSearch->GetDataStatus();
        $data['user'] = $this->MSearch->GetDataUser();
        $this->template->loadview($title, 'master', 'contents', 'pages/Search', $data);
    }

    function SearchDataOrder()
    {
        $data = $this->input->post();
        $dataReturn = $this->MSearch->SearchDataOrder($data["input_search"], $data["user_id"], $data["status_id"]);
        echo json_encode($dataReturn);
    }
}
