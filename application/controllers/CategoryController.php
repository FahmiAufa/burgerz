<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CategoryController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OrderModel', 'MOrder');
        $this->load->model('CategoryModel', 'MCategory');
    }

    public function index()
    {
        $title = "Category";
        $data = array();
        $data["Category"] = $this->MOrder->GetDataCategory();
        $this->template->loadview($title, 'master', 'contents', 'pages/Category', $data);
    }

    public function SaveDataCategory()
    {
        $data = $this->input->post();
        if ($data["category_id"] != null) {
            $arrayData = array(
                "category_id" => $data["category_id"],
                "category_name" => $data["category_name"],
                "category_weight" => $data["category_weight"],
                "is_active" => 1,
                "updated_by" => $this->session->userdata('user_username'),
                "updated_date" => date("Y/m/d h:i:sa")
            );
            $this->MCategory->UpdateDataCategory($arrayData);
        } else {
            $arrayData = array(
                "category_id" => $data["category_id"],
                "category_name" => $data["category_name"],
                "category_weight" => $data["category_weight"],
                "is_active" => 1,
                "created_by" => $this->session->userdata('user_username'),
                "created_date" => date("Y/m/d h:i:sa"),
            );
            $this->MCategory->CreateDataCategory($arrayData);
        }

        echo json_encode("success");
    }

    public function DeleteCategory()
    {
        $data = $this->input->post();
        $this->MCategory->DeleteDataCategory($data["category_id"]);

        echo json_encode("success");
    }
}
