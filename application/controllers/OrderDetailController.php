<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OrderDetailController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $title = "Order Detail";
        $data = array();
        $this->template->loadview($title, 'master', 'contents', 'pages/OrderDetail', $data);

        echo json_encode("success");
    }
}
