<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProductController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProductModel', 'MProduct');
        $this->load->model('OrderModel', 'MOrder');
    }

    public function index()
    {
        $id = $this->input->get("id");
        $title = "Product";
        $data = array();
        $data["Category"] = $this->MOrder->GetDataCategory();
        if ($id == null) {
            $data["product"] = null;
        } else {
            $data["product"] = $this->MProduct->GetDataProduct($id);
        };
        $this->template->loadview($title, 'master', 'contents', 'pages/Product', $data);
    }

    public function CreateProduct()
    {
        $data = $this->input->post();
        $config['upload_path'] = 'assets/images/product/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        // $config['overwrite'] = TRUE;
        // $config['file_name'] = 'logoIMG';
        // $config['file_name'] = $data["product_id"] .$img_name;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('inptImage')) {
            $error = array('error' => $this->upload->display_errors());
            $arrayData = array(
                "product_id" => $data["inptId"],
                "product_name" => $data["inptName"],
                "category_id" => $data["selCatgory"],
                "product_description" => $data["inptDesc"],
                "product_tiny_description" => $data["inptDesc"],
                "product_price" => $data["inptPrice"],
                "product_weight" => $data["inptWeight"],
                "is_active" => 1,
                "updated_by" => $this->session->userdata('user_username'),
                "updated_date" => date("Y/m/d h:i:sa")
            );
            $this->MProduct->UpdateDataProduct($arrayData);
        } else {
            if ($data["inptId"] != null) {
                $img_name = $this->upload->data('file_name');
                $arrayData = array(
                    "product_id" => $data["inptId"],
                    "product_name" => $data["inptName"],
                    "category_id" => $data["selCatgory"],
                    "product_description" => $data["inptDesc"],
                    "product_tiny_description" => $data["inptDesc"],
                    "product_price" => $data["inptPrice"],
                    "product_image" => $img_name,
                    "product_weight" => $data["inptWeight"],
                    "is_active" => 1,
                    "updated_by" => $this->session->userdata('user_username'),
                    "updated_date" => date("Y/m/d h:i:sa")
                );
                $this->MProduct->UpdateDataProduct($arrayData);
            } else {
                $img_name = $this->upload->data('file_name');
                $arrayData = array(
                    "product_id" => null,
                    "product_name" => $data["inptName"],
                    "category_id" => $data["selCatgory"],
                    "product_description" => $data["inptDesc"],
                    "product_tiny_description" => $data["inptDesc"],
                    "product_price" => $data["inptPrice"],
                    "product_image" => $img_name,
                    "product_weight" => $data["inptWeight"],
                    "is_active" => 1,
                    "created_by" => $this->session->userdata('user_username'),
                    "created_date" => date("Y/m/d h:i:sa")
                );
                $this->MProduct->CreateDataProduct($arrayData);
            }

            echo json_encode("success");
        }
    }

    public function DeleteProduct()
    {
        $data = $this->input->post();
        $this->MProduct->DeleteDataProduct($data["product_id"]);

        echo json_encode("success");
    }
}
