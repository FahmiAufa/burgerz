<?php
class OrderStatusModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function GetDataOrder($type)
    {
        if ($type == 1) {
            $this->db->where("tbl_orders.user_id", $this->session->userdata('user_id'));
            $this->db->where("tbl_orders.status_id", $type);
            $this->db->where("tbl_orders.is_active", 1);
            return $this->db->get("tbl_orders")->result_array();
        } else {
            $this->db->where("tbl_orders.user_id", $this->session->userdata('user_id'));
            $this->db->where("tbl_orders.status_id", $type);
            $this->db->where("tbl_orders.is_active", 1);
            return $this->db->get("tbl_orders")->result_array();
        }
    }

    function DeleteDataOrder($orders_id)
    {
        $updateArray = array(
            "is_active" => 0,
            "deleted_by" => $this->session->userdata('user_username'),
            "deleted_date" => date("Y/m/d h:i:sa")
        );
        $this->db->where("tbl_orders.orders_id", $orders_id);
        $this->db->update("tbl_orders", $updateArray);
    }
}
