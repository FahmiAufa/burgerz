<?php
class OrderRecievedModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function GetDataOrder($id)
    {
        $this->db->where("tbl_orders.orders_id", $id);
        $this->db->where("tbl_orders.is_active", 1);
        return $this->db->get("tbl_orders")->result_array();
    }

    function GetDataOrderTrans($id)
    {
        return $this->db->query("SELECT O.*, P.product_price, P.product_name FROM tbl_orders_trans O
            LEFT JOIN tbl_product P ON O.product_id = P.product_id
            WHERE O.is_active = 1 AND P.is_active = 1 AND O.orders_id = " . $id . "
            GROUP BY O.orders_trans_id")->result_array();
    }

    function UpdateDataOrder($status_id, $id)
    {
        $arrayResult = array(
            "status_id" => $status_id,
            "updated_by" => $this->session->userdata('user_username'),
            "updated_date" => date("Y/m/d h:i:sa")
        );
        $this->db->where("tbl_orders.orders_id", $id);
        return $this->db->update("tbl_orders", $arrayResult);
    }

    function DeleteDataOrder($orders_id)
    {
        $updateArray = array(
            "is_active" => 0,
            "deleted_by" => $this->session->userdata('user_username'),
            "deleted_date" => date("Y/m/d h:i:sa")
        );
        $this->db->where("tbl_orders.orders_id", $orders_id);
        $this->db->update("tbl_orders", $updateArray);
    }
}
