<?php
class LoginModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function Check_Registered($where)
    {
        return $this->db->get_where("tbl_user", $where);
    }
    function GetUsers($where)
    {
        $data = $this->db->get_where("tbl_user", $where);
        return  $data->result_array();
    }
}
