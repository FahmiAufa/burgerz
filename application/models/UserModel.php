<?php
class UserModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function GetDataUser()
    {
        $this->db->where("tbl_user.is_active", 1);
        return $this->db->get("tbl_user")->result_array();
    }

    function CreateDataUser($data)
    {
        return $this->db->insert("tbl_user", $data);
    }

    function UpdateDataUser($data)
    {
        $this->db->where("tbl_user.user_id", $data["user_id"]);
        $this->db->update("tbl_user", $data);
    }

    function DeleteDataUser($user_id)
    {
        $updateArray = array(
            "is_active" => 0,
            "deleted_by" => $this->session->userdata('user_username'),
            "deleted_date" => date("Y/m/d h:i:sa")
        );
        $this->db->where("tbl_user.user_id", $user_id);
        $this->db->update("tbl_user", $updateArray);
    }
}
