<?php
class SearchModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function SearchDataOrder($searchinput, $user_id, $status_id)
    {
        $where = '';
        if ($status_id > 0) {
            $where = $where . ' AND status_id in (' . $status_id . ') ';
        }

        if ($user_id > 0) {
            $where = $where . ' AND user_id in (' . $user_id . ') ';
        }
        return $this->db->query("SELECT * FROM tbl_orders WHERE orders_code LIKE '%" . $searchinput . "%' AND is_active = 1" . $where)->result_array();
    }

    function GetDataUser()
    {
        $this->db->where("tbl_user.user_is_admin", 0);
        $this->db->where("tbl_user.is_active", 1);
        return $this->db->get("tbl_user")->result_array();
    }

    function GetDataStatus()
    {
        $this->db->where("tbl_status.is_active", 1);
        return $this->db->get("tbl_status")->result_array();
    }
}
