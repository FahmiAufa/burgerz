<?php
class OrderModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function GetDataCategory()
    {
        $this->db->where("tbl_category.is_active", 1);
        return $this->db->get("tbl_category")->result_array();
    }

    function GetDataProduct()
    {
        $this->db->where("tbl_product.is_active", 1);
        return $this->db->get("tbl_product")->result_array();
    }

    function SaveOrder($data)
    {
        $this->db->insert("tbl_orders", $data);
        $insert_id = $this->db->insert_id();

        $dataUpdate =  array(
            "orders_code" => str_pad($insert_id, 5, '0', STR_PAD_LEFT)
        );
        $this->db->where("tbl_orders.orders_id", $insert_id);
        $this->db->update("tbl_orders", $dataUpdate);

        return $insert_id;
    }

    function SaveOrderTrans($data)
    {
        $this->db->insert("tbl_orders_trans", $data);
    }
}
