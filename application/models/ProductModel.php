<?php
class ProductModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function GetDataProduct($id)
    {
        $this->db->where("tbl_product.product_id", $id);
        $this->db->where("tbl_product.is_active", 1);
        return $this->db->get("tbl_product")->result_array();
    }

    function CreateDataProduct($data)
    {
        return $this->db->insert("tbl_product", $data);
    }

    function UpdateDataProduct($data)
    {
        $this->db->where("tbl_product.product_id", $data["product_id"]);
        $this->db->update("tbl_product", $data);
    }

    function DeleteDataProduct($product_id)
    {
        $updateArray = array(
            "is_active" => 0,
            "deleted_by" => $this->session->userdata('user_username'),
            "deleted_date" => date("Y/m/d h:i:sa")
        );
        $this->db->where("tbl_product.product_id", $product_id);
        $this->db->update("tbl_product", $updateArray);
    }
}
