<?php
class CategoryModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function CreateDataCategory($data)
    {
        return $this->db->insert("tbl_category", $data);
    }

    function UpdateDataCategory($data)
    {
        $this->db->where("tbl_category.category_id", $data["category_id"]);
        $this->db->update("tbl_category", $data);
    }

    function DeleteDataCategory($category_id)
    {
        $updateArray = array(
            "is_active" => 0,
            "deleted_by" => $this->session->userdata('user_username'),
            "deleted_date" => date("Y/m/d h:i:sa")
        );
        $this->db->where("tbl_category.category_id", $category_id);
        $this->db->update("tbl_category", $updateArray);
    }
}
